package py.com.personal.procedures;


import org.voltdb.*;
import org.voltdb.client.ClientResponse;
import org.voltdb.client.ProcedureCallback;
import py.com.personal.exceptions.ExceptionCode;
import py.com.personal.exceptions.MyAppException;


import static py.com.personal.utils.PrintUtils.printResults;

/**
 *  @author Alfredo Barrios
 *  @since 1.0
 *  <p>Simple test VoltDb Stored Procedures
 */
public class AB_ProcedureJava extends VoltProcedure {


    private static final String alumnoTable = "AB_ALUMNO";


    //Declare Inserts
    public final SQLStmt insertAlumno= new SQLStmt(
                "INSERT INTO AB_ALUMNO VALUES (?, ?);");


    public final SQLStmt insertStream = new SQLStmt("INSERT INTO AB_ENTRADA_A_UNIVERSIDAD VALUES (?, now);");


    //Declare Updates
    public final SQLStmt updateNombreById = new SQLStmt(
            "UPDATE AB_ALUMNO SET nombre = ? WHERE id_alumno = ?;");



    //Declare Deletes
    public final SQLStmt deleteAlumnoByID = new SQLStmt("DELETE FROM AB_ALUMNO where id_alumno = ?;");


    // Declare Queries
    public final SQLStmt getNombreById = new SQLStmt(
            "SELECT nombre FROM AB_ALUMNO WHERE id_alumno=?;");


    public final SQLStmt getIdAlumnoByNombre = new SQLStmt(
            "SELECT id_alumno FROM AB_ALUMNO WHERE nombre = ? ORDER BY id_alumno;"
    );


    public final SQLStmt getIdByNombreLike = new SQLStmt(
            "SELECT id_materia FROM AB_MATERIA WHERE nombre like ? ORDER BY id_materia;");



    public final SQLStmt getNombreByMateriaId = new SQLStmt("SELECT nombre FROM AB_MATERIA WHERE id_materia=?");



    public final SQLStmt getIdByNombre = new SQLStmt("SELECT id_materia FROM AB_MATERIA WHERE nombre = ? ORDER BY id_materia");



    public final SQLStmt getAllByAlumno = new SQLStmt("SELECT * FROM AB_ALUMNO where nombre = ? ORDER BY id_alumno limit 100;");


    public final SQLStmt getAllEntradasUniversidad = new SQLStmt("select * from AB_ENTRADAS_POR_ALUMNO where id_alumno= ? limit 100;");



    public VoltTable[] run(String idAlumno, String nombreAlumno, String idMateria, String nombreMateria) throws VoltAbortException {
        // Body of the Stored Procedure ...
        VoltTable[] queryresults = null;

        if(nombreAlumno.length()>50)
            throw new VoltAbortException(alumnoTable +"-->id_alumno-->"+ ExceptionCode.maxLength);

        //Inserts
        voltQueueSQL(insertAlumno,idAlumno, nombreAlumno);
        voltQueueSQL(insertStream,idAlumno);

        //Queries
        voltQueueSQL(getAllByAlumno, nombreAlumno);
        voltQueueSQL(getNombreById,EXPECT_ONE_ROW, idAlumno);
        voltQueueSQL(getIdByNombre, nombreMateria);
        voltQueueSQL(getIdAlumnoByNombre, nombreAlumno);
        voltQueueSQL(getIdByNombreLike,nombreMateria);
        voltQueueSQL(getNombreByMateriaId,idMateria);
        voltQueueSQL(getAllEntradasUniversidad,idAlumno);

        //Update
        voltQueueSQL(updateNombreById, nombreAlumno+"_UPDATE", idAlumno);

        //Delete
        voltQueueSQL(deleteAlumnoByID,idAlumno);

        queryresults = voltExecuteSQL();

        //VoltDB Random getSeededRandomNumberGenerator()
        //VoltDB Date
        //getTransactionTime()*/

        return queryresults;
    }

    public static class ABProcedureCallback implements ProcedureCallback {
        @Override
        public void clientCallback(ClientResponse response) {

            // Make sure the procedure succeeded.
            if (response.getStatus() != ClientResponse.SUCCESS)
                throw new MyAppException("ERR-ABProcecureJava",response.getStatusString(),true);

            printResults(response);
        }
    }
}
