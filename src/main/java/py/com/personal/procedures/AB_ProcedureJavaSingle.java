package py.com.personal.procedures;

import org.voltdb.SQLStmt;
import org.voltdb.VoltProcedure;
import org.voltdb.VoltTable;
import org.voltdb.client.ClientResponse;
import org.voltdb.client.ProcedureCallback;
import py.com.personal.exceptions.ExceptionCode;
import py.com.personal.exceptions.MyAppException;

import static py.com.personal.utils.PrintUtils.printResults;

public class AB_ProcedureJavaSingle  extends VoltProcedure  {


    public final SQLStmt getAllByAlumno = new SQLStmt("SELECT * FROM AB_ALUMNO where nombre like ? ORDER BY id_alumno limit 100;");


    private static final String alumnoTable = "AB_ALUMNO";


    public VoltTable[] run(String key, String nombreAlumno) throws VoltAbortException {

        VoltTable[] queryresults;

        if(nombreAlumno.length()>50)
            throw new VoltAbortException(alumnoTable +"-->id_alumno-->"+ ExceptionCode.maxLength);


        voltQueueSQL(getAllByAlumno, nombreAlumno);

        queryresults = voltExecuteSQL(true);
        return queryresults;
    }


    public static class ABProcedureJavaSingleCallback implements ProcedureCallback {
        @Override
        public void clientCallback(ClientResponse response) {

            // Make sure the procedure succeeded.
            if (response.getStatus() != ClientResponse.SUCCESS)
                throw new MyAppException("ERR-ABProcecureJavaSingle",response.getStatusString(),true);

            printResults(response);
        }
    }
}
