package py.com.personal.clients;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voltdb.client.*;

import static py.com.personal.utils.PrintUtils.printResults;

/**
 * @author Alfredo Barrios
 * @since 1.0
 * VoltDb cluster client, for testing purposes
 */
public class Client {
    private static final Logger logger = LoggerFactory.getLogger(Client.class);


    public static void main(String[] args) {
        try {
            org.voltdb.client.Client client;

            ClientStatusListenerExt mylistener = new ClientStatusListenerExt()
            {
                @Override
                public void connectionLost(String hostname, int port,
                int connectionsLeft,
                ClientStatusListenerExt.DisconnectCause cause)
                {
                    logger.error("A connection to the database has been lost."
                            + "There are {} connections remaining.\n", connectionsLeft);
                }
                @Override
                public void backpressure(boolean status)
                {
                    logger.error("Backpressure from the database "
                            + "is causing a delay in processing requests.");
                }
                @Override
                public void uncaughtException(ProcedureCallback callback,
                                              ClientResponse r, Throwable e)
                {
                    logger.error("An error has occurred in a callback "
                            + "procedure. Check the following stack trace for details. {}",e);

                }
                @Override
                public void lateProcedureResponse(ClientResponse response,
                    String hostname, int port)
                {
                    logger.error("A procedure that timed out on host {} {}"
                            + " has now responded.\n", hostname, port);
                }
            };


            //User Pass from arguments
            ClientConfig clientConfig = new ClientConfig(args[0], args[1], mylistener);
            client = ClientFactory.createClient(clientConfig);
            //To reconnect when fail
            clientConfig.setReconnectOnConnectionLoss(true);


            //Three Nodes of the cluster
            client.createConnection(args[2]);
            client.createConnection(args[3]);
            client.createConnection(args[4]);
            //Execute some sync calls to the stored procedures

            SyncTestClient.execute(client);

            //Execute Async Calls to the stored procedures
            AsyncTestClient.testDefaultAsync(client);

            //Execute Async BulkInserts
            AsyncTestClient.testBulkLoader(client);


            //Wait to the responses
            client.drain();
            client.close();

        } catch (Exception e) {
            logger.error("Client Error: {}",e.getMessage());
            logger.debug("Cliet Error: {}",e);
        }
    }

    public static void defaultProcedureCalls(org.voltdb.client.Client client) {

        try {
            //Call the procedures created by default by Voltdb
            ClientResponse clientResponse = client.callProcedure("AB_ALUMNO.insert", "100", "Alfredo");
            printResults(clientResponse);
            clientResponse = client.callProcedure("AB_ALUMNO.select", "100");
            printResults(clientResponse);
            clientResponse = client.callProcedure("AB_ALUMNO.update", "100", "Luis", "100");
            printResults(clientResponse);
            clientResponse = client.callProcedure("AB_ALUMNO.delete", "100");
            printResults(clientResponse);

            clientResponse = client.callProcedure("AB_MATERIA.insert", "100", "VoltDb");
            printResults(clientResponse);
            clientResponse = client.callProcedure("AB_MATERIA.update", "100", "Voltdb2", "100");
            printResults(clientResponse);
            clientResponse = client.callProcedure("AB_MATERIA.delete", "100");
            printResults(clientResponse);

            clientResponse = client.callProcedure("AB_ASISTENCIA.insert", "1", "1", "5");
            printResults(clientResponse);
            clientResponse = client.callProcedure("AB_ASISTENCIA.update", "1", "1", "6", "1");
            printResults(clientResponse);
            clientResponse = client.callProcedure("AB_ASISTENCIA.delete", "1");
            printResults(clientResponse);

        } catch (Exception e) {
           logger.error("DefaultProcedureCall {}",e.getMessage());
           logger.debug("DefaultProcedureCall {}",e);
        }
    }
}
