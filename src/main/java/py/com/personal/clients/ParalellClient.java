package py.com.personal.clients;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voltdb.VoltTable;
import org.voltdb.client.*;
import py.com.personal.multithreading.AlumnoRunnable;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Alfredo Barrios
 * @since 1.1.0
 *
 * VoltDb Paralell client, execute the tasks in one thread per partition
 */
public class ParalellClient {

    private static final Logger logger = LoggerFactory.getLogger(ParalellClient.class);

    public static void main(String[] args) {
        try {
            org.voltdb.client.Client client;
            ClientStatusListenerExt mylistener = new ClientStatusListenerExt()
            {
                @Override
                public void connectionLost(String hostname, int port,
                                           int connectionsLeft,
                                           ClientStatusListenerExt.DisconnectCause cause)
                {
                    logger.error("A connection to the database has been lost."
                            + "There are {} connections remaining.\n", connectionsLeft);
                }
                @Override
                public void backpressure(boolean status)
                {
                    logger.error("Backpressure from the database "
                            + "is causing a delay in processing requests.");
                }
                @Override
                public void uncaughtException(ProcedureCallback callback,
                                              ClientResponse r, Throwable e)
                {
                    logger.error("An error has occurred in a callback "
                            + "procedure. Check the following stack trace for details. {}",e);

                }
                @Override
                public void lateProcedureResponse(ClientResponse response,
                                                  String hostname, int port)
                {
                    logger.error("A procedure that timed out on host {} {}"
                            + " has now responded.\n", hostname, port);
                }
            };


            //User Pass from arguments
            ClientConfig clientConfig = new ClientConfig(args[0], args[1],mylistener);
            client = ClientFactory.createClient(clientConfig);
            //To reconnect when fail
            clientConfig.setReconnectOnConnectionLoss(true);
            //Three Nodes of the cluster
            client.createConnection(args[2]);
            client.createConnection(args[3]);
            client.createConnection(args[4]);


            VoltTable results[] = client.callProcedure("@GetPartitionKeys", "VARCHAR")
                    .getResults();

            VoltTable keys = results[0];

            //Create a pool of threads with a maximum of N threads
            int n = keys.getRowCount();

            ExecutorService executorService = Executors.newFixedThreadPool(n);

            CountDownLatch latch = new CountDownLatch(n);

            for (int k = 0; k < keys.getRowCount(); k++) {

                String key = keys.fetchRow(k).getString(1);

                logger.info("Partition Key: {}",key);

                Object [] parameters = {key,"Miss%"};

                executorService.submit(new AlumnoRunnable(client, latch,parameters));

            }


            //Wait to all Threads Complete their work
            client.drain();

            latch.await();

            executorService.shutdown();

        }catch (Exception e){
            logger.error("Parallel Client : {}", e.getMessage());
            logger.debug("Parallel Cleint: {}",e);
        }
    }
}
