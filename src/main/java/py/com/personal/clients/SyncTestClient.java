package py.com.personal.clients;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voltdb.client.ClientResponse;
import py.com.personal.exceptions.MyAppException;

import static py.com.personal.clients.Client.defaultProcedureCalls;
import static py.com.personal.utils.PrintUtils.printResults;

/**
 * @author Alfredo Barrios
 * @since 1.0
 *
 * Call the stored procedures Synchronously
 */
public class SyncTestClient {

    private static final Logger logger = LoggerFactory.getLogger(SyncTestClient.class);

    public static void execute(org.voltdb.client.Client client){
         try {

             ClientResponse response = client.callProcedure("AB_ProcedureJava", "100", "Alfredo", "19", "VoltDB");

             ClientResponse responseMulti = client.callProcedure("AB_ProcedureJavaMulti", "100", "Alfredo", "16", "VoltDB2");

             printResults(response);

             printResults(responseMulti);

             defaultProcedureCalls(client);


         }catch (MyAppException e){
             logger.error("Procedure Exception: {}", e);
         }catch (Exception e){
             logger.error("Execute Sync Error: {}", e.getMessage());
             logger.debug("Execute Syc Error: {} ", e);
         }
    }
}
