package py.com.personal.bulkloaders;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voltdb.client.ClientResponse;
import org.voltdb.client.ProcedureCallback;
import org.voltdb.client.VoltBulkLoader.BulkLoaderFailureCallBack;
import org.voltdb.client.VoltBulkLoader.BulkLoaderSuccessCallback;

import java.util.concurrent.atomic.AtomicLong;


/**
 * @author Alfredo Barrios
 * @since 1.1.0
 *
 * Specifies the method and Static classes, for implement  Bulk Inserts
 * */
public class MateriaBulkloader {
    private static final Logger logger = LoggerFactory.getLogger(MateriaBulkloader.class);

    public static AtomicLong errors = new AtomicLong(0);
    public static AtomicLong commits = new AtomicLong(0);


    public static void handleResponse(ClientResponse response) {
        if (response.getStatus() == ClientResponse.SUCCESS) {
            commits.getAndIncrement();
        } else {
            errors.getAndIncrement();
            logger.error("Response Error Status: {}",response.getStatusString());
        }
    }

    // Implement a BulkLoaderFailureCallback for your BulkLoader
    public static class MateriaBulkloaderFailureCallback implements BulkLoaderFailureCallBack {
        @Override
        public void failureCallback(Object rowHandle, Object[] fieldList, ClientResponse cr) {
            handleResponse(cr);
        }
    }


    // Implement a BulkLoaderSuccessCallback for your BulkLoader
    public static class MateriaBulkloaderSuccessCallback implements BulkLoaderSuccessCallback {
        @Override
        public void success(Object rowHandle, ClientResponse cr) {
            handleResponse(cr);
        }
    }


    // Implement ProcedureCallback for asynchronous procedure calls
    public static class DefaultCallback implements ProcedureCallback {
        @Override
        public void clientCallback(ClientResponse cr) {
            handleResponse(cr);
        }
    }

}
