package py.com.personal.procedures;


import org.voltdb.*;
import org.voltdb.client.ClientResponse;
import org.voltdb.client.ProcedureCallback;
import py.com.personal.exceptions.ExceptionCode;
import py.com.personal.exceptions.MyAppException;


import static py.com.personal.utils.PrintUtils.printResults;

/**
 *  @author Alfredo Barrios
 *  @since 1.0
 *  <p>Simple test VoltDb Stored Procedures
 */
public class AB_ProcedureJavaMulti extends VoltProcedure {


    private final String alumnoTable = "AB_ALUMNO";




    // Declare Inserts ..
    public final SQLStmt insertAlumno= new SQLStmt(
            "INSERT INTO AB_ALUMNO VALUES (?, ?);");


    public final SQLStmt insertMateria = new SQLStmt("INSERT INTO AB_MATERIA VALUES (?, ?);");


    public final SQLStmt insertStream = new SQLStmt("INSERT INTO AB_ENTRADA_A_UNIVERSIDAD VALUES (?, now);");


    public final SQLStmt insertAlumnoMateria = new SQLStmt("INSERT INTO AB_ALUMNO_MATERIA VALUES (?,?);");


    //Declare Updates
    public final SQLStmt updateNombreById = new SQLStmt(
            "UPDATE AB_ALUMNO SET nombre = ? WHERE id_alumno = ?;");


    public final SQLStmt updateNombreMateriaById = new SQLStmt("UPDATE AB_MATERIA SET nombre = ? WHERE id_materia = ?");


    public final SQLStmt updateMateriaByAlumno = new SQLStmt("UPDATE AB_ALUMNO_MATERIA SET id_materia = ? WHERE  id_alumno= ?");



    //Declare Deletes
    public final SQLStmt deleteByID = new SQLStmt("DELETE FROM AB_ALUMNO where id_alumno = ?;");


    public final SQLStmt deleteMateriaByID = new SQLStmt("DELETE FROM AB_MATERIA where id_materia = ?;");


    //Declare Queries
    public final SQLStmt getNombreById = new SQLStmt(
            "SELECT nombre FROM AB_ALUMNO WHERE id_alumno=?;");


    public final SQLStmt getIdAlumnoByNombre = new SQLStmt(
            "SELECT id_alumno FROM AB_ALUMNO WHERE nombre = ? ORDER BY id_alumno;"
    );


    public final SQLStmt getIdByNombreLike = new SQLStmt(
            "SELECT id_materia FROM AB_MATERIA WHERE nombre like ? ORDER BY id_materia;");

    public final SQLStmt getNombreByMateriaId = new SQLStmt("SELECT nombre FROM AB_MATERIA WHERE id_materia=?");


    public final SQLStmt getIdByNombre = new SQLStmt("SELECT id_materia FROM AB_MATERIA WHERE nombre = ? ORDER BY id_materia");


    public final SQLStmt getAllByAlumno = new SQLStmt("SELECT * FROM AB_ALUMNO where nombre = ? ORDER BY id_alumno limit 100;");


    public final SQLStmt getAllMateriaByAlumno = new SQLStmt("SELECT id_materia FROM AB_ALUMNO_MATERIA where id_alumno = ? ORDER BY id_materia;");


    public final SQLStmt getAllAlumnoByMateria = new SQLStmt("SELECT id_alumno FROM AB_ALUMNO_MATERIA where id_materia = ? ORDER BY id_alumno;");


    public final SQLStmt getPorcentajeAsistencia = new SQLStmt("SELECT asistencia_porcentaje FROM AB_ASISTENCIA where id_alumno=? ORDER BY id_alumno;");


    public final SQLStmt getAllEntradasUniversidad = new SQLStmt("select * from AB_ENTRADAS_POR_ALUMNO where id_alumno= ? limit 100;");




    public VoltTable[] run(String idAlumno, String nombreAlumno, String idMateria, String nombreMateria) throws VoltAbortException {

        // Body of the Stored Procedure ...
        VoltTable[] queryresults;

        if(nombreAlumno.length()>50)
            throw new VoltAbortException(alumnoTable +"-->id_alumno-->"+ ExceptionCode.maxLength);

        //Inserts
        voltQueueSQL(insertAlumno,idAlumno, nombreAlumno);
        voltQueueSQL(insertStream,idAlumno);
        voltQueueSQL(insertAlumnoMateria,idMateria,idAlumno);
        voltQueueSQL(insertMateria,idMateria, nombreMateria);

        //Queries
        voltQueueSQL(getAllByAlumno, nombreAlumno);
        voltQueueSQL(getNombreById,EXPECT_ONE_ROW, idAlumno);
        voltQueueSQL(getIdByNombre, nombreMateria);
        voltQueueSQL(getIdAlumnoByNombre, nombreAlumno);
        voltQueueSQL(getIdByNombreLike,nombreMateria);
        voltQueueSQL(getNombreByMateriaId,idMateria);
        voltQueueSQL(getAllMateriaByAlumno,idAlumno);
        voltQueueSQL(getAllAlumnoByMateria,idMateria);
        voltQueueSQL(getPorcentajeAsistencia,idAlumno);
        voltQueueSQL(getAllEntradasUniversidad,idAlumno);

        //Updates
        voltQueueSQL(updateNombreMateriaById,nombreMateria+"_UPDATE",idMateria);
        voltQueueSQL(updateMateriaByAlumno,idMateria,idAlumno);
        voltQueueSQL(updateNombreById, nombreAlumno+"_UPDATE", idAlumno);

        //Deletes
        voltQueueSQL(deleteByID,idAlumno);
        voltQueueSQL(deleteMateriaByID,idMateria);

        queryresults = voltExecuteSQL();
        //VoltDB Random getSeededRandomNumberGenerator()
        //VoltDB Date
        //getTransactionTime()
        return queryresults;
    }

    public static class ABProcedureJavaMultiCallback implements ProcedureCallback {
        @Override
        public void clientCallback(ClientResponse response) {

            // Make sure the procedure succeeded.
            if (response.getStatus() != ClientResponse.SUCCESS)
                throw new MyAppException("ERR-ABProcecureJavaMulti",response.getStatusString(),true);


            printResults(response);
        }
    }
}
