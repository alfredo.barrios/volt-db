
--Asegurar de Borrar los Store Procedures que están asociados a la columna nombre
DROP PROCEDURE AB_PROCEDURE_GET_NOMBRE IF EXISTS;
DROP PROCEDURE AB_ProcedureJava IF EXISTS;
DROP PROCEDURE AB_ProcedureJavaMulti IF EXISTS;

--J.I Renombrar columna, antes de eliminar , se debe verificar que los datos ya no se usarán.
-- También se deben eliminar los Stored Procedures asociados.
ALTER TABLE AB_ALUMNO DROP COLUMN nombre CASCADE;
ALTER TABLE AB_MATERIA ADD COLUMN new_nombre VARCHAR(33);

--J.II Reducir el size
--Chequear si se tienen registros de mayor longitud antes de hacer el cambio
SELECT nombre FROM ab_alumno where  CHAR_LENGTH(nombre)>32;
ALTER TABLE AB_MATERIA ALTER COLUMN nombre SET DATA TYPE VARCHAR(32);

--J.III Quitar una columna
ALTER TABLE AB_ASISTENCIA DROP COLUMN asistencia_porcentaje CASCADE;

--J.IV Aumentar el size de un Stream
--ALTER STREAM AB_ENTRADA_A_UNIVERSIDAD ALTER COLUMN id_alumno SET DATA TYPE VARCHAR(40);
--DROP STREAM AB_ENTRADA_A_UNIVERSIDAD;

DROP TABLE AB_ENTRADA_A_UNIVERSIDAD;
--Registro de entada en las puertas de la universidad
CREATE STREAM AB_ENTRADA_A_UNIVERSIDAD PARTITION ON COLUMN id_alumno (
    id_alumno VARCHAR (36) NOT NULL,
    login TIMESTAMP
);


--J.V  Setear como not null(Se agrega, porque no había columnas sin not Null)
ALTER TABLE AB_MATERIA ADD COLUMN new_column VARCHAR(1);
ALTER TABLE AB_MATERIA ALTER COLUMN new_column SET NOT NULL;

--K Particionar tabla
PARTITION TABLE AB_ALUMNO_MATERIA ON COLUMN id_materia;
