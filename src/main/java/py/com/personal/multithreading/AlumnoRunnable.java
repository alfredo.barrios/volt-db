package py.com.personal.multithreading;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.procedures.AB_ProcedureJavaSingle;

import java.util.Arrays;
import java.util.concurrent.CountDownLatch;
/**
 * @author Alfredo Barrios
 * @since 1.1.0
 *
 * Runnable Task for call to the stored procedure AB_ProcedureJavaSingle
 *
 * */
public class AlumnoRunnable implements Runnable {

    private static final Logger logger = LoggerFactory.getLogger(AlumnoRunnable.class);

    // Global state
    final org.voltdb.client.Client client;
    private CountDownLatch latch;
    private final Object [] parameters;

    public AlumnoRunnable(org.voltdb.client.Client client, CountDownLatch latch, Object [] parameters) {
        this.client = client;
        this.latch = latch;
        this.parameters = parameters;
    }

    @Override
    public void run() {
        try {
            if(parameters==null)
            {
                logger.error("Trying to send NULL data to the Stored Procedure");
                return;
            }

            logger.info("Send Data: {}", Arrays.toString(parameters));

            client.callProcedure(new AB_ProcedureJavaSingle.ABProcedureJavaSingleCallback(), "AB_ProcedureJavaSingle", parameters);

            latch.countDown();

        }catch (Exception e){
            logger.error("AppRunnable : {}",e.getMessage());
            logger.debug("AppRunnable : {}", e);
        }
    }
}
