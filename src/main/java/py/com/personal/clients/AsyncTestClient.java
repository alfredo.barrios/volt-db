package py.com.personal.clients;

import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.voltdb.client.VoltBulkLoader.VoltBulkLoader;
import py.com.personal.bulkloaders.AlumnoBulkLoader;
import py.com.personal.bulkloaders.AsistenciaBulkLoader;
import py.com.personal.bulkloaders.EntradaUniversidadStream;
import py.com.personal.bulkloaders.MateriaBulkloader;
import py.com.personal.exceptions.MyAppException;
import py.com.personal.procedures.AB_ProcedureJava;
import py.com.personal.procedures.AB_ProcedureJavaMulti;

import static py.com.personal.bulkloaders.AlumnoBulkLoader.testSize;

/**
 * @author Alfredo Barrios
 * @since 1.1.0
 * Call the stored procedures Asynchronously
 *
 * */
public class AsyncTestClient {


    private static final Logger logger = LoggerFactory.getLogger(AsyncTestClient.class);


    public static void testDefaultAsync(org.voltdb.client.Client client) {
        try {

            // Asynchronous procedure call

            //The call will fail because the Name does not match the regex

            client.callProcedure(new AB_ProcedureJava.ABProcedureCallback(), "AB_ProcedureJava",
                    "20000", "1234Alfredo", "19", "VoltDB");


            client.callProcedure(new AB_ProcedureJavaMulti.ABProcedureJavaMultiCallback(), "AB_ProcedureJavaMulti", "20000", "1234Alfredo", "16", "VoltDB2");

            client.drain();

        } catch (MyAppException e) {

            if (!e.getStatus())
                logger.error("UNKNOWN-ERROR: MyAppException");

            logger.error("Error: {}", e);

        } catch (Exception e) {
            logger.error("TestDefaultAsync: {}", e.getMessage());
            logger.debug("TestDefaultAsync: {}", e);
        }
    }


    public static void testBulkLoader(org.voltdb.client.Client client) {
        try {

            // Get a BulkLoader for the table we want to load, with a given batch size and one callback handles failures for any failed batches
            int batchSize = 1000;

            VoltBulkLoader alumnoBulkLoader = client.getNewBulkLoader("AB_ALUMNO",
                    batchSize,
                    false,
                    new AlumnoBulkLoader.AlumnoBulkloaderFailureCallback(),
                    new AlumnoBulkLoader.AlumnoBulkloaderSuccessCallback());


            VoltBulkLoader entradaUniversidadBulkloader = client.getNewBulkLoader("AB_ENTRADA_A_UNIVERSIDAD",
                    batchSize,
                    false,
                    new EntradaUniversidadStream.EntradaUniversidadFailureCallback(),
                    new EntradaUniversidadStream.EntradaUniversidadSuccessCallback());


            VoltBulkLoader asistenciaBulkloader = client.getNewBulkLoader("AB_ASISTENCIA",
                    batchSize,
                    false,
                    new AsistenciaBulkLoader.AsistenciaBulkloaderFailureCallback(),
                    new AsistenciaBulkLoader.AsistenciaBulkloaderSuccessCallback());


            VoltBulkLoader materiaBulkloader = client.getNewBulkLoader("AB_MATERIA",
                    batchSize,
                    false,
                    new MateriaBulkloader.MateriaBulkloaderFailureCallback(),
                    new MateriaBulkloader.MateriaBulkloaderSuccessCallback());


            long startNanos = System.nanoTime();

            //Library for generate Names

            Faker faker = new Faker();

            for (int i = 500; i < testSize; i++) {

                Object[] rowAlumno = {i, faker.name().fullName()};

                Object[] rowEntradaUniversidadStream = {i, System.currentTimeMillis()};

                Object[] rowAsistencia = {i, i, 100};

                Object[] rowMateria = {i,"VoltDB"};

                alumnoBulkLoader.insertRow(i, rowAlumno);

                entradaUniversidadBulkloader.insertRow(i, rowEntradaUniversidadStream);

                asistenciaBulkloader.insertRow(i, rowAsistencia);

                materiaBulkloader.insertRow(i, rowMateria);

            }

            alumnoBulkLoader.drain();

            entradaUniversidadBulkloader.drain();

            asistenciaBulkloader.drain();

            materiaBulkloader.drain();

            client.drain();

            double elapsedSeconds = (System.nanoTime() - startNanos) / 1000000000.0;

            int tps = (int) ((40000) / elapsedSeconds);

            logger.info("Se cargaron {}, registros en {} seconds ( {} rows/sec)", ((testSize * 4) - 2000), elapsedSeconds, tps);

        } catch (Exception e) {
            logger.error("testBulkLoader: {}", e.getMessage());
            logger.debug("testBulLoader: {} ", e);
        }
    }
}
