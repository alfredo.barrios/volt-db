package py.com.personal.exceptions;

/**
 * @author Alfredo Barrios
 * @since  1.1.0
 * Custom Exception for the application
 * **/
public class MyAppException extends RuntimeException {
    private final String statusCode;
    private final String description;
    private final boolean status;


    public MyAppException(String statusCode, String description, boolean status) {
        super(description);
        this.statusCode = statusCode;
        this.description = description;
        this.status = status;
    }


    public String getStatusCode() {
        return statusCode;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "MyAppException{" +
                "statusCode='" + statusCode + '\'' +
                ", description='" + description + '\'' +
                ", status=" + status +
                '}';
    }
}
