package py.com.personal.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.voltdb.VoltTable;
import org.voltdb.client.ClientResponse;
/**
 * @author Alfredo Barrios
 * @since 1.1.0
 * **/
public class PrintUtils {

    private static final Logger logger = LoggerFactory.getLogger(PrintUtils.class);

    public static void printResults(ClientResponse clientResponse) {

        if (ClientResponse.SUCCESS != clientResponse.getStatus()) {
            logger.error("ClientREsponseError: {}" , clientResponse.getStatusString());
            return;
        }


        VoltTable[] results = clientResponse.getResults();


        for (int i = 0; i < results.length; i++)
            logger.info("VoltTable: {}" , results[i]);


    }
}
