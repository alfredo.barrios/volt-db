--Registro Alumno
CREATE TABLE AB_ALUMNO(
        id_alumno VARCHAR (33) NOT NULL,
        nombre VARCHAR (50) NOT NULL,
        CONSTRAINT AB_PRIKEY_ALUMNO PRIMARY KEY (id_alumno)
);
PARTITION TABLE AB_ALUMNO ON COLUMN id_alumno;

CREATE TABLE AB_MATERIA(
    id_materia VARCHAR (33) NOT NULL,
    nombre VARCHAR (33) NOT NULL,
    CONSTRAINT AB_PRIKEY_MATERIA PRIMARY KEY (id_materia)
);
PARTITION TABLE AB_MATERIA ON COLUMN id_materia;


--Tabla Asistencia
CREATE TABLE AB_ASISTENCIA(
  id_materia_registro VARCHAR(33) NOT NULL,
  id_alumno VARCHAR(33) NOT NULL,
  asistencia_porcentaje VARCHAR (3) NOT NULL,
  CONSTRAINT AB_PRIKEY_ASISTENCIA PRIMARY KEY (id_materia_registro)
);

CREATE UNIQUE INDEX AB_ASISTENCIA_INDEX  ON AB_ASISTENCIA (id_alumno);

--Asociacion entre Alumno y materia
CREATE TABLE AB_ALUMNO_MATERIA(
   id_materia VARCHAR (33) NOT NULL,
   id_alumno VARCHAR(33) NOT NULL,
);

--Registro de entada en las puertas de la universidad
CREATE STREAM AB_ENTRADA_A_UNIVERSIDAD
      PARTITION ON COLUMN id_alumno (
        id_alumno VARCHAR (33) NOT NULL,
        login TIMESTAMP
      );

--Consulta de entradas por alumno
CREATE VIEW AB_ENTRADAS_POR_ALUMNO
    ( id_alumno, total_entradas, ultima_visita)
    AS SELECT id_alumno, COUNT(*), MAX(login)
       FROM AB_ENTRADA_A_UNIVERSIDAD GROUP BY id_alumno;

--GET NOMBRE
CREATE PROCEDURE AB_PROCEDURE_GET_NOMBRE
   PARTITION ON TABLE AB_ALUMNO COLUMN id_alumno
   AS SELECT nombre
      FROM  AB_ALUMNO where id_alumno = ?

--Búsqueda de Id Materia por nombre
CREATE PROCEDURE AB_PROCEDURE_GET_ID_MATERIA_BY_NAME
    AS SELECT id_materia FROM AB_MATERIA
        WHERE nombre like ?

CREATE PROCEDURE
    PARTITION ON TABLE AB_ALUMNO COLUMN id_alumno
       FROM CLASS py.com.personal.procedures.AB_ProcedureJava;

CREATE PROCEDURE
       FROM CLASS py.com.personal.procedures.AB_ProcedureJavaMulti;

CREATE PROCEDURE
       PARTITION ON TABLE AB_ALUMNO COLUMN id_alumno
       FROM CLASS py.com.personal.procedures.AB_ProcedureJavaSingle;
